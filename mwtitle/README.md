mwtitle
=======
[![crates.io](https://img.shields.io/crates/v/mwtitle.svg)](https://lib.rs/crates/mwtitle)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://gitlab.com/mwbot-rs/mwbot/badges/main/coverage.svg)](https://mwbot-rs.gitlab.io/mwbot/coverage/)


The `mwtitle` crate is a library for parsing, normalizing and formatting MediaWiki
page titles. See the [documentation](https://docs.rs/mwtitle) ([main](https://mwbot-rs.gitlab.io/mwbot/mwtitle/))
for more details.

## License
`mwtitle` is released under the GPL v3 or any later version. It contains code
directly copied and ported from MediaWiki by various authors. The Rust parts
were primarily written by Erutuon and Kunal Mehta.
