fn main() {
    // Invalidate build cache when JSON data changes
    println!("cargo:rerun-if-changed=data");
}
