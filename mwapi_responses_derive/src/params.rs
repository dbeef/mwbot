/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::metadata::{default_query_fields, Metadata};
use crate::FieldContainer;
use proc_macro2::TokenStream as TokenStream2;
use quote::{quote, ToTokens};
use std::collections::HashMap;

pub(crate) enum ParsedParams {
    List(String, HashMap<String, String>),
    Props(Vec<String>, HashMap<String, String>),
    None(HashMap<String, String>),
}

impl ParsedParams {
    pub fn new(mut params: HashMap<String, String>) -> Self {
        set_defaults(&mut params);
        // TODO: meta= modules?

        let list = params.get("list").map(|l| l.to_string());
        let props: Vec<_> = match params.get("prop") {
            Some(prop) => prop.split('|').map(|p| p.to_string()).collect(),
            None => vec![],
        };
        if list.is_none() && props.is_empty() {
            ParsedParams::None(params)
        } else if list.is_some() && !props.is_empty() {
            panic!("You can only specify one of list= or prop=, not both");
        } else if let Some(listname) = list {
            ParsedParams::List(listname, params)
        } else {
            ParsedParams::Props(props, params)
        }
    }

    pub(crate) fn get_fieldname(&self) -> String {
        match self {
            ParsedParams::List(listname, _) => {
                Metadata::new(listname).fieldname
            }
            ParsedParams::Props(_, _) | ParsedParams::None(_) => {
                "pages".to_string()
            }
        }
    }

    fn get_params(&self) -> &HashMap<String, String> {
        match self {
            ParsedParams::List(_, params) => params,
            ParsedParams::Props(_, params) => params,
            ParsedParams::None(params) => params,
        }
    }

    fn get_fields_for(&self, module: &str, container: &mut FieldContainer) {
        let info = Metadata::new(module);
        let props: Vec<&str> = match &info.prop {
            Some(prop) => {
                match self.get_params().get(prop) {
                    Some(value) => value.split('|').collect(),
                    None => {
                        // FIXME: implement defaults
                        vec![]
                    }
                }
            }
            None => vec![],
        };
        let wrap_field = if info.wrap_in_vec {
            Some(info.fieldname.to_string())
        } else {
            None
        };
        container.add_fields(wrap_field, info.get_fields(&props));
    }

    pub(crate) fn get_fields(&self) -> FieldContainer {
        let mut container = FieldContainer::default();
        match self {
            ParsedParams::List(listname, _) => {
                self.get_fields_for(listname, &mut container);
            }
            ParsedParams::Props(props, _) => {
                // These are always top-level fields, so add them directly
                container.top.extend(default_query_fields());
                for prop in props {
                    self.get_fields_for(prop, &mut container);
                }
            }
            ParsedParams::None(_) => {
                container.top.extend(default_query_fields());
            }
        }
        container
    }
}

/// Turn the HashMap of params into a slice that returns
/// `&[(&str, &str)]`, which is the format used by mwapi and trivially
/// convertable into a HashMap<String, String> for mediawiki-rs.
impl ToTokens for ParsedParams {
    fn to_tokens(&self, tokens: &mut TokenStream2) {
        let params = self.get_params();
        let keys = params.keys();
        let values = params.values();

        let stream = quote! {
            &[
                #((#keys, #values),)*
            ]
        };
        stream.to_tokens(tokens);
    }
}

fn set_defaults(params: &mut HashMap<String, String>) {
    // Verify action=query
    match params.get("action") {
        Some(val) => {
            if val != "query" {
                panic!("Only action=query is supported");
            }
        }
        None => {
            params.insert("action".to_string(), "query".to_string());
        }
    }

    // Force format=json&formatversion=2
    params.insert("format".to_string(), "json".to_string());
    params.insert("formatversion".to_string(), "2".to_string());
}
