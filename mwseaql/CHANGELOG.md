## 0.1.2 / 2022-10-05
* Re-export `sea_query` for convenience of downstream users.

## 0.1.1 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.1.0 / 2022-09-19
* Initial release.
