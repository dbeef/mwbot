//! Schema definitions for Toolforge-specific tables
use sea_query::Iden;

/// `meta_p.wiki` table
/// Documentation is available on [wikitech.wikimedia.org](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Metadata_database).
#[derive(Iden)]
#[iden = "wiki"]
pub enum Wiki {
    Table,
    #[iden = "dbname"]
    DBName,
    #[iden = "lang"]
    Lang,
    #[iden = "name"]
    Name,
    #[iden = "family"]
    Family,
    #[iden = "url"]
    URL,
    #[iden = "size"]
    Size,
    #[iden = "is_closed"]
    IsClosed,
    #[iden = "has_echo"]
    HasEcho,
    #[iden = "has_flaggedrevs"]
    HasFlaggedRevs,
    #[iden = "has_visualeditor"]
    HasVisualEditor,
    #[iden = "has_wikidata"]
    HasWikidata,
    #[iden = "is_sensitive"]
    IsSensitive,
}
